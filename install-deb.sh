#!/bin/bash

#usersname=$(whoami)

#countdown
echo "script will execute in 10 seconds"
sleep 5s
echo "script will execute in 5 seconds"
sleep 2s 
echo "script wil execute in 3 seconds"
sleep 1s
echo "script will execute in 2 secconds"
sleep 1s
echo "script will execute in 1 seccond"
sleep 1s

git --version || sudo apt-get install git -y

echo "------------------------------------------------------------------------------"
echo "-- Decator Install is now running, please do not cancel during installation --"
echo "------------------------------------------------------------------------------"

############################################################
# Defining functions                                       #
############################################################
function brave_install_debain {
    sudo apt install apt-transport-https curl
    sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
    sudo apt update -y
    sudo apt install brave-browser -y
}

function install_snap_packages {
    sudo snap install snap-store
    sudo snap install okular
    sudo snap install motrix
    sudo snap install obs-studio
}

function install_and_enable_plexmediaserver {
    wget https://downloads.plex.tv/plex-media-server-new/1.25.1.5286-34f965be8/debian/plexmediaserver_1.25.1.5286-34f965be8_amd64.deb
    sudo dpkg -i plexmediaserver_1.25.1.5286-34f965be8_amd64.deb
    dpkg -L plexmediaserver
    sudo systemctl enable plexmediaserver.service && sudo systemctl start plexmediaserver.service
}

 function download_extract_add_fonts {
    truetype=0
    cd ~/ || exit
    git clone https://github.com/El-Wumbus/fonts
    cd fonts || exit
    cd /usr/share/fonts/truetype || truetype=1
    cd ~/fonts || exit
    sudo 7z e ~/fonts/Fonts.7z
    rm -rf Fonts.7z
    cd ~/ || exit
    if [ "$truetype" -gt 0 ]
    then 
        sudo  mv  -v ~/fonts/* /usr/share/fonts/TTF
    else 
        sudo mv  -v ~/fonts/* /usr/share/fonts/truetype/
    fi
    rm -rf Fonts.7z
    cd ..
    rm -rf fonts
    cd ~/ || exit
    cd ~/Decator-deb || exit
}

sudo dpkg --configure -a

############################################################
# Installing Programs                                      #
############################################################

sudo apt clean
sudo apt update
sudo apt dist-upgrade

#updating packages
echo "-------------------------------------------------"
echo "-- updating existing packages and repositories --"
echo "-------------------------------------------------"
sudo apt update -y && sudo apt upgrade -y

sudo apt-get clean

#install
sudo apt install wget -y
sudo apt install curl -y
sudo apt install python -y
sudo apt-get install p7zip-full -y
sudo apt-get install unzip -y
sudo apt install solaar -y
sudo apt install code -y #vscode
sudo apt install spectacle -y
sudo apt install mpv -y
sudo apt install thunderbird -y
sudo apt install terminator -y
sudo apt install samba -y
sudo apt install gnome-disk-utility -y
sudo apt install audacity -y
sudo apt install thunderbird -y
sudo apt install pavucontrol -y
sudo apt install kdenlive
sudo apt install ardour -y
sudo apt install gimp -y
sudo apt install handbrake -y
sudo apt install gedit -y 
sudo apt install wine -y
sudo apt install winetricks -y
sudo apt install discord -y
sudo apt install lutris -y
sudo apt install snapd -y
pip gitpython
pip wxpython
sudo apt-get install cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3 -y
brave_install_debain
install_and_enable_plexmediaserver

#install gitub desktop
wget -qO - https://packagecloud.io/shiftkey/desktop/gpgkey | sudo apt-key add -
Sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/shiftkey/desktop/any/ any main" > /etc/apt/sources.list.d/packagecloud-shiftky-desktop.list'
sudo apt-get update
sudo apt install github-desktop

#install snapd
echo "----------------------------------"
echo "-- installing snap & snap store --"
echo "----------------------------------"
sudo apt update -y
sudo apt install snapd -y
sudo snap install core
install_snap_packages

#install sedja PDF editor
wget https://sejda-cdn.com/downloads/sejda-desktop_7.3.7_amd64.deb
sudo dpkg -i sejda-desktop_7.3.7_amd64.deb

#install fonts
echo "
-------------------------------------------
-- adding fonts to /usr/share/fonts/TTF/ --
-------------------------------------------
"
download_extract_add_fonts

sudo apt-get update && sudo apt-get upgrade && sudo apt-get autoclean

sudo dpkg --configure -a

echo "
----------------------------------------------------------------
-- finished running script, PLEASE REBOOT using 'sudo reboot' --
----------------------------------------------------------------
"
echo "Exit status: "
echo $
echo "run 'sudo nano /etc/apt/sources.list.d/plexmediaserver.list' and uncomment the last line, then run 'wget -q https://downloads.plex.tv/plex-keys/PlexSign.key -O - | sudo apt-key add -
&& sudo apt update -y && sudo apt updrade -y'"
echo "please visit https://el-wumbus.github.io/"
exit
