# Decator-deb
#### _Just a very customizeable program install script for Debian &amp; related Operating systems._
***
Run the script by running the following command in your preferred terminal emulator. **Must Be in the Home Directory**:


```markdown 
cd ~/ && git clone https://github.com/El-Wumbus/Decator-deb && cd Decator-deb && sudo chmod u+x install-deb.sh && ./install-deb.sh
```

***

#### _For the font installation:_
if the '/usr/share/fonts/truetype' directory is not found then, by default, the '/usr/share/fonts/TTF' folder will be used.
